from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from .models import Artist
import spotify
import base64, urllib, httplib, requests,json
from requests.auth import HTTPBasicAuth

APPID = ""
CLIENT_ID = "bfcc8ad591ad4683abb8d49e00b0ab29"
CLIENT_SECRET = "249ef66f5bdc44f0b470cdbb79911436"
state = '123abcukme'
redirect_uri = "https%3A%2F%2Fexample.com%2Fcallback"
code = ""
rec = True

class IndexView(generic.ListView):
	model = Artist
	template_name = 'spotify/index.html'


class ArtistsView(generic.ListView):
	model = Artist
	template_name = 'spotify/artists.html'
	context_object_name = 'artists'

	def get_queryset(self):
		return Artist.objects.all()

def spc2(request):
	global rec 
	rec = True
	return HttpResponseRedirect('https://accounts.spotify.com/authorize/?client_id=bfcc8ad591ad4683abb8d49e00b0ab29&response_type=code&redirect_uri=http%3A%2F%2F127.0.0.1%3A8000%2Fspotify%2FloggedIn&scope=user-follow-read user-read-email&state=34fFs29kd09')

def spotifyConnectView(request):
	global rec 

	rec = False
	# return request.text

	# params = {'client_id':CLIENT_ID,'response_type':'code','redirect_uri':redirect_uri,'state':state,'scope':'user-read-private'}
	# context = {'params': params}
	# return render(request, 'https://accounts.spotify.com/authorize', context)
	return HttpResponseRedirect('https://accounts.spotify.com/authorize/?client_id=bfcc8ad591ad4683abb8d49e00b0ab29&response_type=code&redirect_uri=http%3A%2F%2F127.0.0.1%3A8000%2Fspotify%2FloggedIn&scope=user-follow-read user-read-email&state=34fFs29kd09')
	# return HttpResponseRedirect('https://accounts.spotify.com/authorize/?client_id=bfcc8ad591ad4683abb8d49e00b0ab29&response_type=code&redirect_uri=http%3A%2F%2F127.0.0.1%3A8000%2Fspotify%2Fmy_recommendations&scope=user-follow-read user-read-email&state=34fFs29kd09')

def loggedInView(request):
	global rec
	params = request.GET
	# context = {'access_token' : params.get('access_token'), 'token_type' : params.get('token_type'), 'expires_in' : params.get('expires_in'), 'state' : params.get('state')}
	code = params.get('code')
	state =str(params.get('state'))
	if (rec):
		eventDetails = spotify.getIntoData(str(code), True)
	# titles = results.get('titles')
		zippedData = zip(eventDetails[0], eventDetails[1], eventDetails[2], eventDetails[3], eventDetails[4], eventDetails[5], eventDetails[6], eventDetails[7], eventDetails[8], map(lambda x: int(x)/4, eventDetails[9]), map(lambda x: int(x)/4, eventDetails[10]))
	##RELATED EVENTS HERE
		context = {'events' : zippedData}
		 
		return render(request, 'spotify/my_concerts.html', context)

	eventDetails = spotify.getIntoData(str(code), False)
	# titles = results.get('titles')
	zippedData = zip(eventDetails[0], eventDetails[1], eventDetails[2], eventDetails[3], eventDetails[4], eventDetails[5], eventDetails[6], eventDetails[7], eventDetails[8], map(lambda x: int(x)/4, eventDetails[9]), map(lambda x: int(x)/4, eventDetails[10]))
	##RELATED EVENTS HERE
	context = {'events' : zippedData}

	return render(request, 'spotify/my_concerts.html', context)


def logged2(request):
	params = request.GET
	# context = {'access_token' : params.get('access_token'), 'token_type' : params.get('token_type'), 'expires_in' : params.get('expires_in'), 'state' : params.get('state')}
	code = params.get('code')
	state =str(params.get('state'))
	
	
	eventDetails = spotify.getIntoData(str(code), True)
	# titles = results.get('titles')
	zippedData = zip(eventDetails[0], eventDetails[1], eventDetails[2], eventDetails[3], eventDetails[4], eventDetails[5], eventDetails[6], eventDetails[7], eventDetails[8], map(lambda x: int(x)/4, eventDetails[9]), map(lambda x: int(x)/4, eventDetails[10]))
	##RELATED EVENTS HERE
	context = {'events' : zippedData}

	return render(request, 'spotify/my_recommendations.html', context)

def my_concerts(request, context):
	context = {'titles' : titles, 'dates' : dates, 'descriptions' : descriptions, 'cities' : cities, 'countries' : countries, 'distances' : distances}
	render(request, 'spotify/my_concerts', context)


def spotify_connect_for_recommendations(request):

	return my_recommendations(request)


def my_recommendations(request):
	params = request.GET
	# context = {'access_token' : params.get('access_token'), 'token_type' : params.get('token_type'), 'expires_in' : params.get('expires_in'), 'state' : params.get('state')}
	code = params.get('code')
	eventDetails = spotify.getIntoData(str(code), True)
	# titles = results.get('titles')
	##RELATED EVENTS HERE
	zippedData = zip(eventDetails[0], eventDetails[1], eventDetails[2], eventDetails[3], eventDetails[4], eventDetails[5], eventDetails[6], eventDetails[7], eventDetails[8], map(lambda x: int(x)/4, eventDetails[9]), map(lambda x: int(x)/4, eventDetails[10]))

	#zippedData2 = zip(relatedEventDetails[11], relatedEventDetails[12], relatedEventDetails[13], relatedEventDetails[14], relatedEventDetails[15], relatedEventDetails[16], relatedEventDetails[17], relatedEventDetails[18], relatedEventDetails[19], map(lambda x: int(x)/4, relatedEventDetails[20]), map(lambda x: int(x)/4, relatedEventDetails[21]))
	#context = {'relatedEvents' : zippedData2}
	context = {'events' : zippedData}


	return render(request, 'spotify/my_recommendations.html', context)
















