from django.db import models
import spotify
# Create your models here.

class Artist(models.Model):
	name = models.CharField(max_length=200)
	genre = models.CharField(max_length=200)

	def  __unicode__(self):              # __unicode__ on Python 2
		return self.name

	number_songs = models.IntegerField()

class User(models.Model):
	access_token = models.CharField(max_length=200)
	refresh_token = models.CharField(max_length=200)

