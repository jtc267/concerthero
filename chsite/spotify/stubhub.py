import base64, urllib, httplib, requests, json
from requests.auth import HTTPBasicAuth
# APPTOKEN = "XGV2iR1untJ2bNgK5fw1K7QA4gMa"
# CONSUMER_KEY = "9rnLa5vgCzHWpM1TjBGT5HEFkAIa"
# CONSUMER_SECRET= "R8_Aso4lIRLH_NedSGF8D0u3flka"
APPTOKEN = "UxtIyvEnigKcZzCf9cWR9KdQS7Qa"
CONSUMER_KEY = "fZ7FfU9Bnpk0ijC25xhbkNSX5Hsa"
CONSUMER_SECRET= "thc9HS1xSYWCPPlAH7O1KMu3ba4a"


def connectToStubHub():
	basicAuthToken = CONSUMER_KEY + ":" + CONSUMER_SECRET
	#print basicAuthToken
	# #print "eHl6cEtOYlFPbWZCTzF2cHRmZWZKRU9BYTphYmNHU0Q3ckZrU0NZOUZDZlUwbjk1R1RkZmdVVQ=="

	# #print base64.b64encode("xyzpKNbQOmfBO1vptfefJEOAa:abcGSD7rFkSCY9FCfU0n95GTdfgUU")
	# encoded = base64.b64encode(basicAuthToken)
	# #print encoded
	# headers = {"User-Agent": "Mozilla/5.0","Content-type": "application/x-www-form-urlencoded",
 #           "Authorization": "Basic " + basicAuthToken}
 #    conn = httplib.HTTPConnection("https://api.stubhubsandbox.com/login HTTP/1.1")
	url = 'https://api.stubhub.com/login'
	headers = {"User-Agent": "Mozilla/5.0","Content-type": "application/x-www-form-urlencoded","Authorization": "Basic " + basicAuthToken}
	user = CONSUMER_KEY # sandbox consumer key
	pw = CONSUMER_SECRET # sandbox consumer secret
	data = 'grant_type=password&username=jtcsmash32@gmail.com&password=Orangergrey32'
	r = requests.post( url, auth=HTTPBasicAuth(user, pw), data = data, headers=headers )
	cookies = r.cookies
	print cookies
	print r.text
	return r


def getEvent(r, givenNames,picProperties):
	# picProperties is url, height, width tuple.
	# return (givenNames)
	temp = map(lambda x: "\"" + x + "\"", givenNames)
	names = ' |'.join(temp)
	coordIthaca = '42.4433, -76.5000'
	cookies = r.cookies
	requestsArr = []
	
	for name in temp:
		requestsArr.append(requests.get('https://api.stubhub.com/search/catalog/events/v2?categoryName=Concert&status=Active&performerName=' + name + '&point=' + coordIthaca + '&units=mi&radius=240&sort=dateLocal asc', cookies=cookies, headers={'Authorization': 'Bearer ' + APPTOKEN}))
	
	
	parsedStuff = []
	tiedEvents = []
	NOE = []
	countArtists = -1;
	# status=Active&performerName=' + names + '&point=' + coordIthaca + '&units=mi&radius=120&sort=dateLocal desc
	for request in requestsArr:
		countArtists+=1
		parsed = json.loads(request.text)
		parsedStuff.append(parsed)
		numFound = parsed['numFound']
		counter = 0
		date2 = ""

		if (int(str(numFound)) > 0):
			date2 = ""
			events = parsed['events']
			for event in events:
				# NOE.append(event)
				eventtitle = str(event['title'])
				date = str(event['dateUTC'][0:10])
				
				if (date == date2): 
					tiedEvents.append(eventtitle)
					tiedEvents.append(event2title)
					continue
				date2 = str(event['dateUTC'][0:10])
				event2title = str(event['title'])
				event['picURL'] = picProperties[countArtists][0]
				event['picHeight'] = picProperties[countArtists][1]
				event['picWidth'] = picProperties[countArtists][2]
				# all artists venues that aren't on the same day - 1 event per artist per day max
				NOE.append(event)
		
	sortNOE = sorted(NOE,key=lambda event:str(event['dateUTC'][0:10]))
	# return sortNOE
	# if (NOE):
	# 			else:
	# 				for datedEvent in NOE:
	# 					if (str(datedEvent['dateUTC'][0:10]) >= str(event['dateUTC'][0:10])):
	# 						NOE.append(event)
	# 						break
	# 					NOE.append(event)
	eventTitles = []
	eventDates= []
	eventDesc = []
	eventVenues= []
	eventCities= []
	eventStates= []
	eventCountries= []
	eventInfoUrls = []

	eventPicUrls = []
	eventPicHeights = []
	eventPicWidths = []

	if (sortNOE):
		for event in sortNOE:
			venue = event['venue']
			# counter+=1
			eventTitles.append(str(event['attributes'][0]['value']))
			eventDates.append(str(event['dateUTC'][0:10]))			
			eventDesc.append(str(event['title']) + " at " + str(venue['name']))
			# eventDesc.append(str(event['description']))
			eventCities.append(str(venue['city']))
			eventStates.append(str(venue['state']))
			eventCountries.append(str(venue['country']))
			eventVenues.append(str(venue['name']))
			eventInfoUrls.append(str(event['eventInfoUrl']))
			eventPicUrls.append(str(event['picURL']))
			eventPicHeights.append(str(event['picHeight']))
			eventPicWidths.append(str(event['picWidth']))

	eventDetails = []
	eventDetails.append(eventTitles)					
	eventDetails.append(eventDates)					
	eventDetails.append(eventDesc)					
	eventDetails.append(eventCities)					
	eventDetails.append(eventStates)					
	eventDetails.append(eventCountries)					
	eventDetails.append(eventVenues)	
	eventDetails.append(eventInfoUrls)
	eventDetails.append(eventPicUrls)
	eventDetails.append(eventPicHeights)
	eventDetails.append(eventPicWidths)

	# eventDetails.append(eventTitles)					


	# r2 = requests.get('https://api.stubhub.com/search/catalog/events/v2?categoryName=Concert', cookies=cookies, headers={'Authorization': 'Bearer ' + APPTOKEN})
	#r2 = requests.get(   'https://api.stubhub.com/search/inventory/v1?eventid=9107490', cookies=cookies, headers={'Authorization': 'Bearer ' + APPTOKEN})
	# print r2
	# r3 = requests.get('https://api.stubhub.com/search/catalog/events/v2?categoryName=Concert', cookies=cookies, headers={'Authorization': 'Bearer ' + APPTOKEN})

	# parsed = json.loads(r2.text)
	# parsed3 = json.loads(r3.text)

	# # print parsed
	# num=0
	# numFound = parsed['numFound']
	# events = parsed['events']
	# eventTitles= []	
	# eventDates= []
	# eventDesc = []
	# events3 = parsed3['events']
	# eventTitles3= []	
	# eventDates3= []
	# eventDesc3 = []

	# # way to get around stupid stubhub limit - make multiple queries, one for each artist
	# for i in range(1,9):
	# 	num+=1
	# 	event = events[i]
	# 	event3 = events3[i]
	# 	eventTitles.append(str(event['title']))
	# 	eventTitles3.append(str(event3['title']))

	# 	eventDates.append(str(event['dateUTC'][0:10]))
	# 	eventDesc.append(str(event['description']))

	return eventDetails
	# print r2.cookies
	# artist = "Wilco"
	# params = {}
	# url
	# headers = {"User-Agent": "Mozilla/5.0","Content-type": "application/x-www-form-urlencoded","Authorization": "Bearer " + APPTOKEN}


# getEvent("Wilco")