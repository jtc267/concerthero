from django.conf.urls import url

from . import views
import spotify

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^artists', views.ArtistsView.as_view(), name='artists'),
    url(r'^spotify_connect', views.spotifyConnectView, name='spotify_connect'),
    url(r'^loggedIn', views.loggedInView, name='loggedIn'),
    url(r'^logged2', views.logged2, name='logged2'),

    url(r'^my_concerts', views.my_concerts, name='my_concerts'),
    # url(r'^spotify_connect_for_recommendations', views.poo, name="poo"),
    url(r'^spc2', views.spc2, name="spotify_connect_for_recommendations"),
    url(r'^my_recommendations', views.my_recommendations, name='my_recommendations')
    # url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    # url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
]