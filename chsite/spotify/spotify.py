import base64, urllib, httplib, requests,json, urllib2, unicodedata
from requests.auth import HTTPBasicAuth
import stubhub

APPID = ""
CLIENT_ID = "bfcc8ad591ad4683abb8d49e00b0ab29"
CLIENT_SECRET = "249ef66f5bdc44f0b470cdbb79911436"
state = '123abcukme'
redirect_uri = "http%3A%2F%2F127.0.0.1%3A8000%2Fspotify%2FloggedIn"

def connectToSpotify():
	# redirect_uri = "concerthero.co"
	params = {'client_id':CLIENT_ID,'response_type':'code','redirect_uri':redirect_uri,'state':state,'scope':'user-read-private'}
	url = 'https://accounts.spotify.com/authorize'
	r = requests.get(url,params=params)
	print r
	# return r.text


def getIntoData(code, recommended):
	# print r.text
	# data = r.text
	# code = ""
	# nState = data.get('state')
	# if ((nState) == state):
	# 	x = 1	
	# else:
	# return recommended
	#note: recommended is a boolean item for whether item is for recommended artists or not
	uri_for_redirection = redirect_uri
	# if recommended:
	# 	uri_for_redirection = "http%3A%2F%2F127.0.0.1%3A8000%2Fspotify%2Fmy_recommendations"

	url = "https://accounts.spotify.com/api/token"
	params = {"grant_type":"authorization_code","code":code,"redirect_uri":uri_for_redirection,"client_id":CLIENT_ID,"client_secret":CLIENT_SECRET}
	data = urllib.urlencode(params)
	req = urllib2.Request(url, data)
	response = urllib2.urlopen(req)
	r2 = response.read()
	tokens = json.loads(r2)

	access_token = tokens['access_token']
	token_type = tokens["token_type"]
	refresh_token = tokens["refresh_token"]

	# HERE IS WHERE WE CREATE A USER

	headers = {"Authorization": "Bearer " + str(access_token)}
	params = {'type' : 'artist','limit' : '50'}
	url2 = "https://api.spotify.com/v1/me/following"
	# req2 = urllib2.Request(url)
	# req2.add_header('Authorization',"Bearer " + str(access_token))
	# resp = urllib2.urlopen(req2)
	r3 = requests.get(url2, params = params, headers = headers)
	artists = json.loads(r3.text)
	inside = artists['artists']
	items = inside['items']
	names = []
	ids = []
	picProperties = []
	picPropertiesRelated = []
	dicPR = {}
	picUrls = []
	relatedArtists = {}
	for item in items:
		artistid = item['id']
		relatedResponse = requests.get("https://api.spotify.com/v1/artists/" + artistid + "/related-artists")
		rA = json.loads(relatedResponse.text)['artists']
		for relatedartist in rA:
			name = relatedartist['name']
			name = unicodedata.normalize('NFKD', name).encode('ascii','ignore')
			images = relatedartist['images']
			tup = ("",0,0)
			if (len(images) > 0):
				height = str(images[0]['height'])
				url = str(images[0]['url'])
				width = str(images[0]['width'])
				tup = (url,height,width)
			# else:
			# 	picPropertiesRelated.append(("",0,0))



			if name in relatedArtists:
				relatedArtists[name] +=1

			else:
				relatedArtists[name] = 1
				dicPR[name] = tup
		# rpic = requests.get("https://api.spotify.com/v1/artists/" + artistid)
		# artistInfo = json.loads(rpic.text)
		images = item['images']
		if (len(images) > 0):
			height = str(images[0]['height'])
			url = str(images[0]['url'])
			width = str(images[0]['width'])
			picProperties.append((url,height,width))
		else:
			picProperties.append(("",0,0))
		nm = item['name']
		names.append(unicodedata.normalize('NFKD', nm).encode('ascii','ignore'))
	



	sortedRelatedByFrequency = sorted(relatedArtists, key=relatedArtists.get, reverse=True)
	aFewRelated = sortedRelatedByFrequency[0:min(20,len(sortedRelatedByFrequency))]
	for artist in aFewRelated:
		picPropertiesRelated.append(dicPR[artist])

	#stubhubresponseRelated = stubhub.connectToStubHub()
	stubhubresponse = stubhub.connectToStubHub()

	if recommended:

		events = stubhub.getEvent(stubhubresponse, aFewRelated,picPropertiesRelated)
		return events
	else:
		events = stubhub.getEvent(stubhubresponse, names,picProperties)
		return events
	# return picProperties + [str(len(picProperties))]

# getIntoData(connectToSpotify())