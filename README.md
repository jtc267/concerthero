# README #
Jacob Cooper (jtc267@cornell.edu) & Kurt Shuster (kls294@cornell.edu)
9/20/15

This repo holds our code for an 'intelligent' concert recommending website (done for BigRedHacks hackathon).

When you hook up your Spotify account, it takes all the liked artists that you have and creates a list of all of the concerts within a certain radius of your location. Funnily enough, Spotify now has a very similar feature on their app, but we beat them to it!!

For this project, we used several different APIs to get concert data & liked artists, and we also used Django to deploy it on a temporary website given to us at the hackathon, which is now expired and has been taken down.

Also, the newest code has been broken when we were trying to make improvements and when my workload for school picked up, I still haven't had the time to fix it and actually upload it to a website.

Jacob